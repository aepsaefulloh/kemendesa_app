
var routes = [
  {
    path: '/',
    url: './index.html',
  },
  {
    path: '/post/',
    componentUrl: './pages/post.html',
  },
  {
    path: '/post-detail/:productId/',
    async: function ({ router, to, resolve }) {
      // App instance
      var app = router.app;

      // Show Preloader
      app.preloader.show();

      // Product ID from request
      var productId = to.params.productId;

      // Simulate Ajax Request
      setTimeout(function () {
        // We got user data from request
        var productDetail = [];
        app.request.json('https://staging.sketsahouse.com/ckl/api/public/api/product/' + productId)
          .then(function (response) {
            productDetail = response.data.data;
            console.log('di Route ', productDetail);
            // Hide Preloader
            app.preloader.hide();
            // Resolve route to load page
            resolve(
              {
                componentUrl: './pages/post-detail.html',
              },
              {
                props: {
                  product: productDetail,
                }
              }
            );
          });
      }, 1000);
    }
  },
  {
    path: '/category/',
    componentUrl: './pages/category.html',
  },
  {
    path: '/explore/',
    url: './pages/explore.html',
  },
  {
    path: '/chat/',
    url: './pages/chat.html',
  },
  {
    path: '/profile/',
    componentUrl: './pages/profile.html',
  },
  {
    path: '/login/',
    componentUrl: './pages/login.html',
  },
  {
    path: '/splash/',
    url: './pages/splash.html',
  },
  // Default route (404 page). MUST BE THE LAST
  {
    path: '(.*)',
    url: './pages/404.html',
  },
];
