
app.request.json('https://staging.sketsahouse.com/ckl/api/public/api/category/product')
.then(function (response) {
  var category = response.data.data;
//   console.log(category);
  var html = '';
  for (var i = 0; i < category.length; i++) {
    // html += '<li>' + category[i].title + '</li>';
    html += '<div class="swiper-slide">'+
    '<div class="box-category">' +
      '<div class="box-image">' +
        '<img src="assets/images/category/category-2.jpg" class="img-fluid" alt="">'+
      '</div>'+
      '<div class="box-content">'+
        '<div class="title">'+category[i].TITLE+'</div>'+
      '</div>'+
    '</div>'+
    '</div>';
  }
  $('#category').html(html);
});

app.request.json('https://staging.sketsahouse.com/ckl/api/public/api/product')
.then(function (response) {
  var recommend = response.data.data;
  // console.log(recommend);
  html = '';
  for (var i = 0; i < recommend.length; i++) {
    html += '<li>' +
      '<a href="/post-detail/'+recommend[i].UNIQ_CODE+'/" class="item-link item-content no-padding">' +
        '<div class="item-media">' +
          '<img src="'+recommend[i].IMAGE.THUMB+'" class="img-fluid" />' +
        '</div>' +
        '<div class="item-inner">' +
          '<div class="item-title-row no-padding-right">' +
            '<div class="item-subtitle">'+recommend[i].CATEGORY_NAME+'</div>' +
            '<div class="item-after icon"><i class="f7-icons">bookmark</i></div>' +
          '</div>' +
          '<div class="item-title">'+recommend[i].NAME_PRODUCT+'</div>' +
          '<div class="item-title-row no-padding-right meta">' +
            '<div class="item-text author">Mrs.Dahlia S,Sos., MSI</div>' +
            '<div class="item-after icon review"><i class="f7-icons">star_fill</i>' +
              '<span>4.9</span>' +
            '</div>' +
          '</div>' +
        '</div>' +
      '</a>' +
    '</li>';
  }
  $('#recommend').html(html);
});
 