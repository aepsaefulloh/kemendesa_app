app.request.json('https://staging.sketsahouse.com/ckl/api/public/api/product')
.then(function (response) {
  var recommend = response.data.data;
  // console.log(recommend);
  html = '';
  for (var i = 0; i < recommend.length; i++) {
    html += '<li>' +
      '<a href="/post-detail/'+recommend[i].UNIQ_CODE+'" class="item-link item-content no-padding">' +
        '<div class="item-media">' +
          '<img src="'+recommend[i].IMAGE.THUMB+'" class="img-fluid" />' +
        '</div>' +
        '<div class="item-inner">' +
          '<div class="item-title-row no-padding-right">' +
            '<div class="item-subtitle">'+recommend[i].CATEGORY_NAME+'</div>' +
            '<div class="item-after icon"><i class="f7-icons">bookmark</i></div>' +
          '</div>' +
          '<div class="item-title">'+recommend[i].NAME_PRODUCT+'</div>' +
          '<div class="item-title-row no-padding-right meta">' +
            '<div class="item-text author">Mrs.Dahlia S,Sos., MSI</div>' +
            '<div class="item-after icon review"><i class="f7-icons">star_fill</i>' +
              '<span>4.9</span>' +
            '</div>' +
          '</div>' +
        '</div>' +
      '</a>' +
    '</li>';
  }
  $('#recommend').html(html);
});