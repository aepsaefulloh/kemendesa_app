

var createStore = Framework7.createStore;
const store = createStore({
  state: {
    apiUrl: 'https://staging.sketsahouse.com/ckl/api/public/api/',
    products: [],
  },
  getters: {
    products({ state }) {
      return state.products;
    },
  },
  actions: {
    getProducts({ state }) {
      return app.request.json(state.apiUrl + 'product')
        .then(function (response) {
          state.products = response.data.data;
          // console.log(state.products);
        });
    },
  },
})

